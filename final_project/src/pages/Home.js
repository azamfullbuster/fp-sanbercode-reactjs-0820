import React from 'react'
import GameList from '../pages/games/GameList'
import MovieList from '../pages/movies/MovieList'

const Home = () => {
    return (
        <>
            <MovieList />
            <GameList />
        </>
    )
}

export default Home